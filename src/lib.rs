//！#siphan
//! A encode & decode lib
//!一个简单的加密库，与一般的加密库不同，Siphan 可以保证其安全性
//! # 基本思路
//! Siphan 的出发点是加密中最基本的凯撒加密法，即生成内置字典的一个随机排序进行比对替换加密。
//! 为了更提升安全性，Siphan 还针对字符不同淡定位置再次进行偏移，如果是 UTF8 字符，还将进行解码编码运算，最后生成一个乱序的字典表和一个密文。
//! # 安全性论证
//! Siphan 内置了 84 个字符，安装原理加密，可以生成的密钥共有 84! 种类，这是一个 127 位数，而且在采用了 UTF8 编码之后，密钥不正确将无法被解码，更保证了安全性。
//!

mod urlcode;
pub use urlcode::*;

mod decode;
mod encode;

pub mod rand;

pub use decode::*;
pub use encode::{Encode, EncodeFrom, EncodeResult};

/// 默认配置的转义字符串内容
pub const ORIGIN_KEY: &'static str =
    "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ,.?!:;+-*/^@0123456789%<>#$&=_|()";

/// 判断字符串全是 ascii 字符构成
pub fn is_all_ascii(s: &str) -> bool {
    !s.chars().any(|c| c.len_utf8() != 1)
}
/// 解码出错
#[derive(Debug, PartialEq, Eq)]
pub struct DecodeError(String);

impl Into<String> for DecodeError {
    fn into(self) -> String {
        self.0
    }
}
pub trait EncodeTrait<T> {
    fn encode(_: T) -> String;
}

pub trait DecodeTrait<T> {
    fn decode(_: T) -> Result<String, DecodeError>;
}
